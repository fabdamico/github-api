package com.twill.assessment.github.repository;

import com.twill.assessment.github.model.GitHubUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GitHubUserRepository extends MongoRepository<GitHubUser, String> {

    /**
     * Find user by username
     *
     * @param login of the github user
     * @return {@link GitHubUser}
     */
    GitHubUser findByLogin(String login);

}
