package com.twill.assessment.github.repository;

import com.twill.assessment.github.model.GitHubRepo;
import com.twill.assessment.github.model.GitHubUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GitHubReposRepository extends MongoRepository<GitHubRepo, String> {

    /**
     * Find all user repositories by username
     *
     * @param owner of the github repo
     * @return {@link GitHubUser}
     */
    List<GitHubRepo> findAllReposByOwner(String owner);
}
