package com.twill.assessment.github;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoClientFactoryBean;

@Configuration
public class ContextConfig {

    /*
     * Factory bean for com.mongodb.Mongo instance
     */
    @Bean
    public MongoClientFactoryBean mongo() {
        return new MongoClientFactoryBean();
    }


}
