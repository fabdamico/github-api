package com.twill.assessment.github.service;

import com.twill.assessment.github.exception.GitHubRepoAlreadyExistsException;
import com.twill.assessment.github.exception.GitHubReposNotFoundException;
import com.twill.assessment.github.exception.GitHubUserNotFoundException;
import com.twill.assessment.github.exception.SequenceException;
import com.twill.assessment.github.model.GitHubRepo;
import com.twill.assessment.github.model.GitHubUser;

import java.util.List;

/**
 * Service layer exposed to the controller to retrieve and manage {@link GitHubUser} user/repo information
 */
public interface GitHubService {

    GitHubUser getUserByUsername(String username) throws GitHubUserNotFoundException;

    List<GitHubRepo> getAllRepoByUsername(String username) throws GitHubReposNotFoundException, GitHubUserNotFoundException;

    boolean createUser(GitHubUser gitHubUser) throws SequenceException;

    boolean createRepo(GitHubRepo gitHubRepo) throws GitHubUserNotFoundException, SequenceException, GitHubRepoAlreadyExistsException, GitHubReposNotFoundException;
}
