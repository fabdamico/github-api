package com.twill.assessment.github.service.impl;

import com.twill.assessment.github.exception.GitHubRepoAlreadyExistsException;
import com.twill.assessment.github.exception.GitHubReposNotFoundException;
import com.twill.assessment.github.exception.GitHubUserNotFoundException;
import com.twill.assessment.github.exception.SequenceException;
import com.twill.assessment.github.model.GitHubRepo;
import com.twill.assessment.github.model.GitHubUser;
import com.twill.assessment.github.repository.GitHubReposRepository;
import com.twill.assessment.github.repository.GitHubUserRepository;
import com.twill.assessment.github.service.AutoIncrementService;
import com.twill.assessment.github.service.GitHubService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Service implementation of {@link GitHubService} layer exposed to the controller to retrieve github user information
 *
 * @author fabdamico.dev@gmail.com
 */

@Slf4j
@Service
public class GitHubServiceImpl implements GitHubService {

    private static final String GITHUB_USER_NOT_FOUND_IN_THE_DATABASE = "Github user {} not found in the database";
    private static final String USER_NOT_FOUND = "User not found";
    private GitHubUserRepository gitHubUserRepository;
    private GitHubReposRepository gitHubReposRepository;
    private AutoIncrementService autoIncrementService;

    @Autowired
    public GitHubServiceImpl(GitHubUserRepository gitHubUserRepository, GitHubReposRepository gitHubReposRepository, AutoIncrementService autoIncrementService) {
        this.gitHubUserRepository = gitHubUserRepository;
        this.gitHubReposRepository = gitHubReposRepository;
        this.autoIncrementService = autoIncrementService;
    }

    @Override
    public GitHubUser getUserByUsername(String username) throws GitHubUserNotFoundException {

        GitHubUser gitHubUser = gitHubUserRepository.findByLogin(username);
        if (gitHubUser == null) {
            log.info(GITHUB_USER_NOT_FOUND_IN_THE_DATABASE, username);
            throw new GitHubUserNotFoundException(USER_NOT_FOUND);
        } else {
            return gitHubUser;
        }
    }

    @Override
    public List<GitHubRepo> getAllRepoByUsername(String username) throws GitHubReposNotFoundException, GitHubUserNotFoundException {

        GitHubUser gitHubUser = gitHubUserRepository.findByLogin(username);

        if (gitHubUser == null) {
            log.info(GITHUB_USER_NOT_FOUND_IN_THE_DATABASE, username);
            throw new GitHubUserNotFoundException(USER_NOT_FOUND);
        }

        List<GitHubRepo> existingRepos = gitHubReposRepository.findAllReposByOwner(username)
                .stream().filter(repo -> repo.getOwner().equals(username))
                .collect(Collectors.toList());

        if (CollectionUtils.isEmpty(existingRepos)) {
            log.warn("No repositories for the user {} ", username);
            throw new GitHubReposNotFoundException("No repositories for the given user");
        } else {
            return existingRepos;
        }
    }

    @Override
    public boolean createUser(GitHubUser newGitHubUser) throws SequenceException {
        GitHubUser existingGitHubUser = gitHubUserRepository.findByLogin(newGitHubUser.getLogin());
        if (existingGitHubUser == null) {
            newGitHubUser.setCreatedAt(Instant.now());
            newGitHubUser.setId(autoIncrementService.getNextSequence("users"));
            gitHubUserRepository.save(newGitHubUser);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean createRepo(GitHubRepo gitHubRepoFromInput) throws GitHubUserNotFoundException, SequenceException, GitHubRepoAlreadyExistsException, GitHubReposNotFoundException {
        String ownerFromInput = gitHubRepoFromInput.getOwner();
        GitHubUser existingGitHubUser = gitHubUserRepository.findByLogin(ownerFromInput);

        if (existingGitHubUser == null) {
            log.info(GITHUB_USER_NOT_FOUND_IN_THE_DATABASE, ownerFromInput);
            throw new GitHubUserNotFoundException("Owner " + ownerFromInput + " doesn't exists");
        } else {

            List<GitHubRepo> existingReposWithSameName = checkIfRepoWithSameNameExists(gitHubRepoFromInput, ownerFromInput);

            if (CollectionUtils.isNotEmpty(existingReposWithSameName)) {
                throw new GitHubRepoAlreadyExistsException(gitHubRepoFromInput.getName() + " repo already exists in the system");
            } else {
                gitHubRepoFromInput.setId(autoIncrementService.getNextSequence("repos"));
                gitHubRepoFromInput.setCreatedAt(Instant.now());
                gitHubRepoFromInput.setOwner(existingGitHubUser.getLogin());
                gitHubReposRepository.save(gitHubRepoFromInput);
                return true;
            }
        }
    }

    private List<GitHubRepo> checkIfRepoWithSameNameExists(GitHubRepo gitHubRepoFromInput, String ownerFromInput) {
        return gitHubReposRepository.findAllReposByOwner(ownerFromInput)
                .stream()
                .filter(repo -> repo.getName().equals(gitHubRepoFromInput.getName()))
                .collect(Collectors.toList());
    }
}
