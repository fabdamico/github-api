package com.twill.assessment.github.service;

import com.twill.assessment.github.exception.SequenceException;
import com.twill.assessment.github.model.SequenceId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;


/**
 * Service that provide autoincrement for the given collection. It use the {@link SequenceId}
 */
@Service
public class AutoIncrementService {

    @Autowired
    private MongoOperations mongoOperations;

    public long getNextSequence(String seqName) throws SequenceException {
        SequenceId sequenceId = mongoOperations.findAndModify(query(where("_id").is(seqName)), new Update().inc("seq", 1),
                options().returnNew(true).upsert(true),
                SequenceId.class);

        if (sequenceId == null) {
            throw new SequenceException("Error while get sequence id for key:" + seqName);
        }

        return sequenceId.getSeq();
    }
}
