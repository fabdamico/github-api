package com.twill.assessment.github.controller;

import com.twill.assessment.github.exception.*;
import com.twill.assessment.github.model.GitHubRepo;
import com.twill.assessment.github.model.GitHubUser;
import com.twill.assessment.github.service.GitHubService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api.github.com/users/")
public class GitHubController {

    private GitHubService gitHubService;

    @Autowired
    public GitHubController(GitHubService gitHubService) {
        this.gitHubService = gitHubService;
    }

    @GetMapping(value = "/{username}")
    public GitHubUser getGitHubUserByUsername(@PathVariable String username) throws GitHubUserNotFoundException {
        return gitHubService.getUserByUsername(username);
    }

    @GetMapping(value = "/{username}/repos")
    public List<GitHubRepo> getAllGitHubRepoByUsername(@PathVariable String username) throws GitHubReposNotFoundException, GitHubUserNotFoundException {
        return gitHubService.getAllRepoByUsername(username);
    }

    @PostMapping(value = "/{username}")
    public ResponseEntity<String> createGitHubUser(@RequestBody @Valid GitHubUser gitHubUser, BindingResult bindingResult) throws SequenceException, GitHubUserAlreadyExistsException, InvalidInputException {

        if (bindingResult.hasErrors()) {
            FieldError fieldError = bindingResult.getFieldErrors().get(0);
            throw new InvalidInputException("Error on field '" + fieldError.getField() + "': rejected value [" + fieldError.getRejectedValue() + "]");
        }

        if (gitHubService.createUser(gitHubUser)) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } else {
            log.error("GitHub user with this username {} is already present in the database", gitHubUser.getLogin());
            throw new GitHubUserAlreadyExistsException("Github user " + gitHubUser.getLogin() + " already exists");
        }
    }

    @PostMapping(value = "/repos")
    public ResponseEntity<String> createGitHubRepo(@RequestBody @Valid GitHubRepo gitHubRepo, BindingResult bindingResult) throws SequenceException, GitHubUserNotFoundException, GitHubRepoAlreadyExistsException, GitHubReposNotFoundException, InvalidInputException {

        if (bindingResult.hasErrors()) {
            FieldError fieldError = bindingResult.getFieldErrors().get(0);
            throw new InvalidInputException("Error on field '" + fieldError.getField() + "': rejected value [" + fieldError.getRejectedValue() + "]");
        }

        String username = gitHubRepo.getOwner();
        if (gitHubService.createRepo(gitHubRepo)) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } else {
            log.error("GitHub user with this username {} is not present in the database", username);
            throw new GitHubUserNotFoundException("GitHub user " + username + " doesn't exists");
        }
    }
}
