package com.twill.assessment.github.exception;

/**
 * Custom Exception to handle error when is impossible to read a sequence id
 */
public class SequenceException extends Exception {

    public SequenceException(String message) {
        super(message);
    }
}
