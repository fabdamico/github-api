package com.twill.assessment.github.exception;

/**
 * Custom exception thrown when the input from the client is not correct
 */
public class InvalidInputException extends Exception {

    public InvalidInputException(String message) {
        super(message);
    }
}
