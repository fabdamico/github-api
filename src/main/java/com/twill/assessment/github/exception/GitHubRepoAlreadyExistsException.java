package com.twill.assessment.github.exception;

/**
 * Custom Exception to manage if a repo with the same name already exists
 */
public class GitHubRepoAlreadyExistsException extends Exception {

    public GitHubRepoAlreadyExistsException(String message) {
        super(message);
    }
}
