package com.twill.assessment.github.exception;

import com.twill.assessment.github.model.JsonError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * Global exception handler to handle all exceptions and map it into HTTP response.
 *
 * @author fabdamico.dev@gmail.com
 */

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(GitHubUserNotFoundException.class)
    public @ResponseBody
    ResponseEntity<JsonError> handleGitHubUserNotFoundException(HttpServletRequest request, Throwable ex) {
        log.error("GitHubUserNotFoundException handler executed");
        return new ResponseEntity<>(new JsonError(ex.getMessage(), request.getRequestURI()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(GitHubReposNotFoundException.class)
    public @ResponseBody
    ResponseEntity<JsonError> handleGitHubReposNotFoundException(HttpServletRequest request, Throwable ex) {
        log.error("GitHubReposNotFoundException handler executed");
        return new ResponseEntity<>(new JsonError(ex.getMessage(), request.getRequestURI()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(GitHubUserAlreadyExistsException.class)
    public @ResponseBody
    ResponseEntity<JsonError> handleGitHubUserAlreadyExistsException(HttpServletRequest request, Throwable ex) {
        log.error("GitHubUserAlreadyExistsException handler executed");
        return new ResponseEntity<>(new JsonError(ex.getMessage(), request.getRequestURI()), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(GitHubRepoAlreadyExistsException.class)
    public @ResponseBody
    ResponseEntity<JsonError> handleGitHubRepoAlreadyExistsException(HttpServletRequest request, Throwable ex) {
        log.error("GitHubRepoAlreadyExistsException handler executed");
        return new ResponseEntity<>(new JsonError(ex.getMessage(), request.getRequestURI()), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(SequenceException.class)
    public @ResponseBody
    ResponseEntity<JsonError> handleSequenceException(HttpServletRequest request, Throwable ex) {
        log.error("SequenceException handler executed");
        return new ResponseEntity<>(new JsonError(ex.getMessage(), request.getRequestURI()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(InvalidInputException.class)
    public @ResponseBody
    ResponseEntity<JsonError> handleInvalidInputException(HttpServletRequest request, Throwable ex) {
        log.error("InvalidInputException handler executed");
        return new ResponseEntity<>(new JsonError(ex.getMessage(), request.getRequestURI()), HttpStatus.BAD_REQUEST);
    }



}
