package com.twill.assessment.github.exception;

/**
 * Custom exception that represent the absence of the github user in the database
 */
public class GitHubUserNotFoundException extends Exception {

    public GitHubUserNotFoundException(String message) {
        super(message);
    }

}
