package com.twill.assessment.github.exception;

/**
 * Custom Exception to manage if a user with the same name already exists
 */
public class GitHubUserAlreadyExistsException extends Exception {

    public GitHubUserAlreadyExistsException(String message) {
        super(message);
    }
}
