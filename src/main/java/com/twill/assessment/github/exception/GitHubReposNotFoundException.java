package com.twill.assessment.github.exception;

/**
 * Exception to manage if a user does not have any repo
 */
public class GitHubReposNotFoundException extends Exception {

    public GitHubReposNotFoundException(String message) {
        super(message);
    }
}
