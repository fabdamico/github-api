package com.twill.assessment.github.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Data
@Document(collection = "repos")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GitHubRepo {

    @Id
    @JsonProperty("id")
    private long id;
    @NotEmpty
    @JsonProperty("name")
    private String name;
    @JsonProperty("full_name")
    private String fullName;
    @NotEmpty
    @JsonProperty("owner")
    private String owner;
    @JsonProperty("private")
    private boolean isPrivate;
    @JsonProperty("html_url")
    private String htmlUrl;
    @JsonProperty("description")
    private String description;
    @JsonProperty("fork")
    private boolean fork;
    @NotEmpty
    @JsonProperty("url")
    private String url;
    @JsonProperty("forks_url")
    private String forksUrl;
    @JsonProperty("keys_url")
    private String keysUrl;
    @JsonProperty("collaborators_url")
    private String collaboratorsUrl;
    @JsonProperty("teams_url")
    private String teamsUrl;
    @JsonProperty("hooks_url")
    private String hooksUrl;
    @JsonProperty("issue_events_url")
    private String issueEventsUrl;
    @JsonProperty("events_url")
    private String eventsUrl;
    @JsonProperty("assignees_url")
    private String assigneesUrl;
    @JsonProperty("branches_url")
    private String branchesUrl;
    @JsonProperty("tags_url")
    private String tagsUrl;
    @JsonProperty("blobs_url")
    private String blobsUrl;
    @JsonProperty("git_tags_url")
    private String gitTagsUrl;
    @JsonProperty("git_refs_url")
    private String gitRefsUrl;
    @JsonProperty("trees_url")
    private String treesUrl;
    @JsonProperty("statuses_url")
    private String statusesUrl;
    @JsonProperty("languages_url")
    private String languagesUrl;
    @JsonProperty("stargazers_url")
    private String stargazersUrl;
    @JsonProperty("contributors_url")
    private String contributorsUrl;
    @JsonProperty("subscribers_url")
    private String subscribersUrl;
    @JsonProperty("subscription_url")
    private String subscriptionUrl;
    @JsonProperty("commits_url")
    private String commitsUrl;
    @JsonProperty("git_commits_url")
    private String gitCommitsUrl;
    @JsonProperty("comments_url")
    private String commentsUrl;
    @JsonProperty("issue_comment_url")
    private String issueCommentUrl;
    @JsonProperty("contents_url")
    private String contentsUrl;
    @JsonProperty("compare_url")
    private String compareUrl;
    @JsonProperty("merges_url")
    private String mergesUrl;
    @JsonProperty("archive_url")
    private String archiveUrl;
    @JsonProperty("downloads_url")
    private String downloadsUrl;
    @JsonProperty("issues_url")
    private String issuesUrl;
    @JsonProperty("pulls_url")
    private String pullsUrl;
    @JsonProperty("milestones_url")
    private String milestonesUrl;
    @JsonProperty("notifications_url")
    private String notificationsUrl;
    @JsonProperty("labels_url")
    private String labelsUrl;
    @JsonProperty("releases_url")
    private String releasesUrl;
    @JsonProperty("deployments_url")
    private String deploymentsUrl;
    @JsonProperty("created_at")
    private Instant createdAt;
    @JsonProperty("updated_at")
    private Instant updatedAt;
    @JsonProperty("pushed_at")
    private Instant pushedAt;
    @JsonProperty("git_url")
    private String gitUrl;
    @JsonProperty("ssh_url")
    private String sshUrl;
    @JsonProperty("clone_url")
    private String cloneUrl;
    @JsonProperty("svn_url")
    private String svnUrl;
    @JsonProperty("homepage")
    private Object homepage;
    @JsonProperty("size")
    private long size;
    @JsonProperty("stargazers_count")
    private long stargazersCount;
    @JsonProperty("watchers_count")
    private long watchersCount;
    @JsonProperty("language")
    private String language;
    @JsonProperty("has_issues")
    private boolean hasIssues;
    @JsonProperty("has_downloads")
    private boolean hasDownloads;
    @JsonProperty("has_wiki")
    private boolean hasWiki;
    @JsonProperty("has_pages")
    private boolean hasPages;
    @JsonProperty("forks_count")
    private long forksCount;
    @JsonProperty("mirror_url")
    private String mirrorUrl;
    @JsonProperty("open_issues_count")
    private long openIssuesCount;
    @JsonProperty("forks")
    private long forks;
    @JsonProperty("open_issues")
    private long openIssues;
    @JsonProperty("watchers")
    private long watchers;
    @JsonProperty("default_branch")
    private String defaultBranch;
    @JsonProperty("network_count")
    private long networkCount;
    @JsonProperty("subscribers_count")
    private long subscribersCount;


    public GitHubRepo id(long id) {
        this.id = id;
        return this;
    }

    public GitHubRepo name(String name) {
        this.name = name;
        return this;
    }

    public GitHubRepo fullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public GitHubRepo owner(String owner) {
        this.owner = owner;
        return this;
    }

    public GitHubRepo htmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
        return this;
    }

    public GitHubRepo description(String description) {
        this.description = description;
        return this;
    }

    public GitHubRepo fork(boolean fork) {
        this.fork = fork;
        return this;
    }

    public GitHubRepo url(String url) {
        this.url = url;
        return this;
    }

    public GitHubRepo forksUrl(String forksUrl) {
        this.forksUrl = forksUrl;
        return this;
    }

    public GitHubRepo keysUrl(String keysUrl) {
        this.keysUrl = keysUrl;
        return this;
    }

    public GitHubRepo collaboratorsUrl(String collaboratorsUrl) {
        this.collaboratorsUrl = collaboratorsUrl;
        return this;
    }

    public GitHubRepo teamsUrl(String teamsUrl) {
        this.teamsUrl = teamsUrl;
        return this;
    }

    public GitHubRepo hooksUrl(String hooksUrl) {
        this.hooksUrl = hooksUrl;
        return this;
    }

    public GitHubRepo issueEventsUrl(String issueEventsUrl) {
        this.issueEventsUrl = issueEventsUrl;
        return this;
    }

    public GitHubRepo eventsUrl(String eventsUrl) {
        this.eventsUrl = eventsUrl;
        return this;
    }

    public GitHubRepo assigneesUrl(String assigneesUrl) {
        this.assigneesUrl = assigneesUrl;
        return this;
    }

    public GitHubRepo branchesUrl(String branchesUrl) {
        this.branchesUrl = branchesUrl;
        return this;
    }

    public GitHubRepo tagsUrl(String tagsUrl) {
        this.tagsUrl = tagsUrl;
        return this;
    }

    public GitHubRepo blobsUrl(String blobsUrl) {
        this.blobsUrl = blobsUrl;
        return this;
    }

    public GitHubRepo gitTagsUrl(String gitTagsUrl) {
        this.gitTagsUrl = gitTagsUrl;
        return this;
    }

    public GitHubRepo gitRefsUrl(String gitRefsUrl) {
        this.gitRefsUrl = gitRefsUrl;
        return this;
    }

    public GitHubRepo treesUrl(String treesUrl) {
        this.treesUrl = treesUrl;
        return this;
    }

    public GitHubRepo statusesUrl(String statusesUrl) {
        this.statusesUrl = statusesUrl;
        return this;
    }

    public GitHubRepo languagesUrl(String languagesUrl) {
        this.languagesUrl = languagesUrl;
        return this;
    }

    public GitHubRepo stargazersUrl(String stargazersUrl) {
        this.stargazersUrl = stargazersUrl;
        return this;
    }

    public GitHubRepo contributorsUrl(String contributorsUrl) {
        this.contributorsUrl = contributorsUrl;
        return this;
    }

    public GitHubRepo subscribersUrl(String subscribersUrl) {
        this.subscribersUrl = subscribersUrl;
        return this;
    }

    public GitHubRepo subscriptionUrl(String subscriptionUrl) {
        this.subscriptionUrl = subscriptionUrl;
        return this;
    }

    public GitHubRepo commitsUrl(String commitsUrl) {
        this.commitsUrl = commitsUrl;
        return this;
    }

    public GitHubRepo gitCommitsUrl(String gitCommitsUrl) {
        this.gitCommitsUrl = gitCommitsUrl;
        return this;
    }

    public GitHubRepo commentsUrl(String commentsUrl) {
        this.commentsUrl = commentsUrl;
        return this;
    }

    public GitHubRepo issueCommentUrl(String issueCommentUrl) {
        this.issueCommentUrl = issueCommentUrl;
        return this;
    }

    public GitHubRepo contentsUrl(String contentsUrl) {
        this.contentsUrl = contentsUrl;
        return this;
    }

    public GitHubRepo compareUrl(String compareUrl) {
        this.compareUrl = compareUrl;
        return this;
    }

    public GitHubRepo mergesUrl(String mergesUrl) {
        this.mergesUrl = mergesUrl;
        return this;
    }

    public GitHubRepo archiveUrl(String archiveUrl) {
        this.archiveUrl = archiveUrl;
        return this;
    }

    public GitHubRepo downloadsUrl(String downloadsUrl) {
        this.downloadsUrl = downloadsUrl;
        return this;
    }

    public GitHubRepo issuesUrl(String issuesUrl) {
        this.issuesUrl = issuesUrl;
        return this;
    }

    public GitHubRepo pullsUrl(String pullsUrl) {
        this.pullsUrl = pullsUrl;
        return this;
    }

    public GitHubRepo milestonesUrl(String milestonesUrl) {
        this.milestonesUrl = milestonesUrl;
        return this;
    }

    public GitHubRepo notificationsUrl(String notificationsUrl) {
        this.notificationsUrl = notificationsUrl;
        return this;
    }

    public GitHubRepo labelsUrl(String labelsUrl) {
        this.labelsUrl = labelsUrl;
        return this;
    }

    public GitHubRepo releasesUrl(String releasesUrl) {
        this.releasesUrl = releasesUrl;
        return this;
    }

    public GitHubRepo deploymentsUrl(String deploymentsUrl) {
        this.deploymentsUrl = deploymentsUrl;
        return this;
    }

    public GitHubRepo createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public GitHubRepo updatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public GitHubRepo pushedAt(Instant pushedAt) {
        this.pushedAt = pushedAt;
        return this;
    }

    public GitHubRepo gitUrl(String gitUrl) {
        this.gitUrl = gitUrl;
        return this;
    }

    public GitHubRepo sshUrl(String sshUrl) {
        this.sshUrl = sshUrl;
        return this;
    }

    public GitHubRepo cloneUrl(String cloneUrl) {
        this.cloneUrl = cloneUrl;
        return this;
    }

    public GitHubRepo svnUrl(String svnUrl) {
        this.svnUrl = svnUrl;
        return this;
    }

    public GitHubRepo homepage(Object homepage) {
        this.homepage = homepage;
        return this;
    }

    public GitHubRepo size(long size) {
        this.size = size;
        return this;
    }

    public GitHubRepo stargazersCount(long stargazersCount) {
        this.stargazersCount = stargazersCount;
        return this;
    }

    public GitHubRepo watchersCount(long watchersCount) {
        this.watchersCount = watchersCount;
        return this;
    }

    public GitHubRepo language(String language) {
        this.language = language;
        return this;
    }

    public GitHubRepo hasIssues(boolean hasIssues) {
        this.hasIssues = hasIssues;
        return this;
    }

    public GitHubRepo hasDownloads(boolean hasDownloads) {
        this.hasDownloads = hasDownloads;
        return this;
    }

    public GitHubRepo hasWiki(boolean hasWiki) {
        this.hasWiki = hasWiki;
        return this;
    }

    public GitHubRepo hasPages(boolean hasPages) {
        this.hasPages = hasPages;
        return this;
    }

    public GitHubRepo forksCount(long forksCount) {
        this.forksCount = forksCount;
        return this;
    }

    public GitHubRepo mirrorUrl(String mirrorUrl) {
        this.mirrorUrl = mirrorUrl;
        return this;
    }

    public GitHubRepo openIssuesCount(long openIssuesCount) {
        this.openIssuesCount = openIssuesCount;
        return this;
    }

    public GitHubRepo forks(long forks) {
        this.forks = forks;
        return this;
    }

    public GitHubRepo openIssues(long openIssues) {
        this.openIssues = openIssues;
        return this;
    }

    public GitHubRepo watchers(long watchers) {
        this.watchers = watchers;
        return this;
    }

    public GitHubRepo defaultBranch(String defaultBranch) {
        this.defaultBranch = defaultBranch;
        return this;
    }

    public GitHubRepo networkCount(long networkCount) {
        this.networkCount = networkCount;
        return this;
    }

    public GitHubRepo subscribersCount(long subscribersCount) {
        this.subscribersCount = subscribersCount;
        return this;
    }
}
