package com.twill.assessment.github.model;

import lombok.Data;


/**
 * Json view in case of error
 *
 * @author fabdamico.dev@gmail.com
 */
@Data
public class JsonError {

    private String message;
    private String path;

    public JsonError(String message, String path) {
        this.message = message;
        this.path = path;
    }
}
