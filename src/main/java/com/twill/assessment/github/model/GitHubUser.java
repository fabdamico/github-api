package com.twill.assessment.github.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "users")
public class GitHubUser {

    @Id
    @JsonProperty("login")
    @NotEmpty
    private String login;

    @Indexed(unique = true)
    @JsonProperty("id")
    private long id;

    @JsonProperty("avatar_url")
    private String avatarUrl;

    @JsonProperty("gravatar_id")
    private String gravatarId;

    @NotEmpty
    @JsonProperty("url")
    private String url;

    @JsonProperty("html_url")
    private String htmlUrl;

    @JsonProperty("followers_url")
    private String followersUrl;

    @JsonProperty("following_url")
    private String followingUrl;

    @JsonProperty("gists_url")
    private String gistsUrl;

    @JsonProperty("starred_url")
    private String starredUrl;

    @JsonProperty("subscriptions_url")
    private String subscriptionsUrl;

    @JsonProperty("organizations_url")
    private String organizationsUrl;

    @JsonProperty("repos_url")
    private String reposUrl;

    @JsonProperty("events_url")
    private String eventsUrl;

    @JsonProperty("received_events_url")
    private String receivedEventsUrl;

    @JsonProperty("type")
    private String type;

    @JsonProperty("site_admin")
    private boolean siteAdmin;

    @JsonProperty("name")
    private String name;

    @JsonProperty("company")
    private String company;

    @JsonProperty("blog")
    private String blog;

    @JsonProperty("location")
    private String location;

    @NotEmpty
    @JsonProperty("email")
    private String email;

    @JsonProperty("hireable")
    private boolean hireable;

    @JsonProperty("bio")
    private String bio;

    @JsonProperty("private_repos")
    private long privateRepos;

    @JsonProperty("private_gists")
    private long privateGists;

    @JsonProperty("followers")
    private long followers;

    @JsonProperty("following")
    private long following;

    @JsonProperty("created_at")
    private Instant createdAt;

    @JsonProperty("updated_at")
    private Instant updatedAt;


    public GitHubUser login(String login) {
        this.login = login;
        return this;
    }

    public GitHubUser id(long id) {
        this.id = id;
        return this;
    }

    public GitHubUser avatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
        return this;
    }

    public GitHubUser gravatarId(String gravatarId) {
        this.gravatarId = gravatarId;
        return this;
    }

    public GitHubUser url(String url) {
        this.url = url;
        return this;
    }

    public GitHubUser htmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
        return this;
    }

    public GitHubUser followersUrl(String followersUrl) {
        this.followersUrl = followersUrl;
        return this;
    }

    public GitHubUser followingUrl(String followingUrl) {
        this.followingUrl = followingUrl;
        return this;
    }

    public GitHubUser gistsUrl(String gistsUrl) {
        this.gistsUrl = gistsUrl;
        return this;
    }

    public GitHubUser starredUrl(String starredUrl) {
        this.starredUrl = starredUrl;
        return this;
    }

    public GitHubUser subscriptionsUrl(String subscriptionsUrl) {
        this.subscriptionsUrl = subscriptionsUrl;
        return this;
    }

    public GitHubUser organizationsUrl(String organizationsUrl) {
        this.organizationsUrl = organizationsUrl;
        return this;
    }

    public GitHubUser reposUrl(String reposUrl) {
        this.reposUrl = reposUrl;
        return this;
    }

    public GitHubUser eventsUrl(String eventsUrl) {
        this.eventsUrl = eventsUrl;
        return this;
    }

    public GitHubUser receivedEventsUrl(String receivedEventsUrl) {
        this.receivedEventsUrl = receivedEventsUrl;
        return this;
    }

    public GitHubUser type(String type) {
        this.type = type;
        return this;
    }

    public GitHubUser siteAdmin(boolean siteAdmin) {
        this.siteAdmin = siteAdmin;
        return this;
    }

    public GitHubUser name(String name) {
        this.name = name;
        return this;
    }

    public GitHubUser company(String company) {
        this.company = company;
        return this;
    }

    public GitHubUser blog(String blog) {
        this.blog = blog;
        return this;
    }

    public GitHubUser location(String location) {
        this.location = location;
        return this;
    }

    public GitHubUser email(String email) {
        this.email = email;
        return this;
    }

    public GitHubUser hireable(boolean hireable) {
        this.hireable = hireable;
        return this;
    }

    public GitHubUser bio(String bio) {
        this.bio = bio;
        return this;
    }

    public GitHubUser privateRepos(long privateRepos) {
        this.privateRepos = privateRepos;
        return this;
    }

    public GitHubUser privateGists(long privateGists) {
        this.privateGists = privateGists;
        return this;
    }

    public GitHubUser followers(long followers) {
        this.followers = followers;
        return this;
    }

    public GitHubUser following(long following) {
        this.following = following;
        return this;
    }

    public GitHubUser createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public GitHubUser updatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }
}
