package com.twill.assessment.github.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Pojo representing sequence document
 */
@Data
@Document(collection = "sequences")
public class SequenceId {
    /**
     * id of the target collection where use the autoincrement
     */
    @Id
    private String id;
    /**
     * autoincrement field for the target id
     **/
    private long seq;
}
