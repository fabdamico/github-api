package com.twill.assessment.github.service.impl;

import com.twill.assessment.github.exception.GitHubRepoAlreadyExistsException;
import com.twill.assessment.github.exception.GitHubReposNotFoundException;
import com.twill.assessment.github.exception.GitHubUserNotFoundException;
import com.twill.assessment.github.exception.SequenceException;
import com.twill.assessment.github.model.GitHubRepo;
import com.twill.assessment.github.model.GitHubUser;
import com.twill.assessment.github.repository.GitHubReposRepository;
import com.twill.assessment.github.repository.GitHubUserRepository;
import com.twill.assessment.github.service.AutoIncrementService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GitHubServiceImplTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();
    @Mock
    private GitHubUserRepository gitHubUserRepository;
    @Mock
    private GitHubReposRepository gitHubReposRepository;
    @Mock
    private AutoIncrementService autoIncrementService;
    private List<GitHubRepo> gitHubRepoList;
    @InjectMocks
    private GitHubServiceImpl gitHubService;

    @Before
    public void setUp() throws Exception {
        gitHubRepoList = new ArrayList<>();
        gitHubRepoList.add(new GitHubRepo().owner("Fabio").name("rest client"));
        gitHubRepoList.add(new GitHubRepo().owner("Fabio").name("soap client"));
    }

    @Test
    public void getUserByUsername_should_thrown_GitHubUserNotFoundException_when_user_is_not_present() throws GitHubUserNotFoundException {
        when(gitHubUserRepository.findByLogin(anyString())).thenReturn(null);
        exception.expect(GitHubUserNotFoundException.class);
        gitHubService.getUserByUsername("User");
    }

    @Test
    public void getUserByUsername_should_return_an_user() throws GitHubUserNotFoundException {
        when(gitHubUserRepository.findByLogin(anyString())).thenReturn(new GitHubUser().login("User"));
        GitHubUser user = gitHubService.getUserByUsername("User");
        verify(gitHubUserRepository, atLeastOnce()).findByLogin(anyString());
        assertThat(user, is(notNullValue()));
        assertThat(user.getLogin(), is("User"));
    }

    @Test
    public void getAllRepoByUsername_should_thrown_GitHubUserNotFoundException_when_user_is_not_present() throws GitHubReposNotFoundException, GitHubUserNotFoundException {
        when(gitHubUserRepository.findByLogin(anyString())).thenReturn(null);
        exception.expect(GitHubUserNotFoundException.class);
        gitHubService.getAllRepoByUsername("UserNotPresent");
    }

    @Test
    public void getAllRepoByUsername_should_return_all_repos_for_given_user() throws GitHubReposNotFoundException, GitHubUserNotFoundException {
        when(gitHubUserRepository.findByLogin(anyString())).thenReturn(new GitHubUser().login("Fabio"));
        when(gitHubReposRepository.findAllReposByOwner(anyString())).thenReturn(gitHubRepoList);
        List<GitHubRepo> fabio_repos = gitHubService.getAllRepoByUsername("Fabio");
        verify(gitHubReposRepository, atLeastOnce()).findAllReposByOwner(anyString());
        verify(gitHubUserRepository, atLeastOnce()).findByLogin(anyString());
        assertThat(fabio_repos.size(), is(2));
    }

    @Test
    public void getAllRepoByUsername_should_thrown_GitHubReposNotFoundException_when_user_has_no_repo() throws GitHubReposNotFoundException, GitHubUserNotFoundException {
        when(gitHubUserRepository.findByLogin(anyString())).thenReturn(new GitHubUser().login("Fabio"));
        when(gitHubReposRepository.findAllReposByOwner(anyString())).thenReturn(Collections.emptyList());
        exception.expect(GitHubReposNotFoundException.class);
        gitHubService.getAllRepoByUsername("Fabio");
    }

    @Test
    public void createUser_should_return_false_if_user_is_already_present() throws SequenceException {
        final String EXISTING_USER = "Existing user";
        when(gitHubUserRepository.findByLogin(anyString())).thenReturn(new GitHubUser().login(EXISTING_USER));
        boolean isExistingUser = gitHubService.createUser(new GitHubUser().login(EXISTING_USER));
        verify(gitHubUserRepository, atLeastOnce()).findByLogin(EXISTING_USER);
        assertThat(isExistingUser, is(false));
    }

    @Test
    public void createUser_should_return_true_if_user_is_already_present() throws SequenceException {
        final String NEW_USER = "New user";
        when(gitHubUserRepository.findByLogin(anyString())).thenReturn(null);
        boolean isExistingUser = gitHubService.createUser(new GitHubUser().login(NEW_USER));
        verify(gitHubUserRepository, atLeastOnce()).findByLogin(NEW_USER);
        verify(autoIncrementService, atLeastOnce()).getNextSequence(anyString());
        assertThat(isExistingUser, is(true));
    }

    @Test
    public void createRepo_should_thrown_GitHubUserNotFoundException_if_the_owner_does_not_exists() throws SequenceException, GitHubUserNotFoundException, GitHubReposNotFoundException, GitHubRepoAlreadyExistsException {
        when(gitHubUserRepository.findByLogin(anyString())).thenReturn(null);
        exception.expect(GitHubUserNotFoundException.class);
        gitHubService.createRepo(new GitHubRepo().owner("OwnerUnknown"));
    }

    @Test
    public void createRepo_should_thrown_GitHubRepoAlreadyExistsException_if_the_repo_with_same_name_exists() throws SequenceException, GitHubUserNotFoundException, GitHubReposNotFoundException, GitHubRepoAlreadyExistsException {
        when(gitHubUserRepository.findByLogin(anyString())).thenReturn(new GitHubUser().login("Fabio"));
        when(gitHubReposRepository.findAllReposByOwner(anyString())).thenReturn(gitHubRepoList);
        exception.expect(GitHubRepoAlreadyExistsException.class);
        gitHubService.createRepo(new GitHubRepo().name("rest client").owner("Fabio"));
    }

    @Test
    public void createRepo_should_return_true_and_create_the_repo() throws SequenceException, GitHubUserNotFoundException, GitHubReposNotFoundException, GitHubRepoAlreadyExistsException {
        GitHubRepo repoToSave = new GitHubRepo().name("new client").owner("Fabio");
        when(gitHubUserRepository.findByLogin(anyString())).thenReturn(new GitHubUser().login("Fabio"));
        when(gitHubReposRepository.findAllReposByOwner(anyString())).thenReturn(gitHubRepoList);
        boolean isRepoCreated = gitHubService.createRepo(repoToSave);
        verify(autoIncrementService, atLeastOnce()).getNextSequence(anyString());
        verify(gitHubReposRepository, atLeastOnce()).save(repoToSave);
        assertThat(isRepoCreated, is(true));
    }

}