# GitHub API

GitHub is RESTful api using Spring-boot and MongoDB to create fake GitHub user and repo.
# Feature available in version 0.0.1
  - Create user
  - Retrieve user by username
  - Create repo
  - Retrieve all repos by username
### Installation

GitHub API requires [MongoDB](https://www.mongodb.com/download-center#community) and [Java SE8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

Install the artifact and and start the server.

```sh
$ cd github-api/
$ mvn clean package
$ cd target/
$ java -jar github-api-0.0.1.jar
```
the application is using default mongo port `27017`, in case you want change port please change the corresponding port in `application.yml`

### Endpoint

- GET localhost:8080/api.github.com/users/{username}/
- GET localhost:8080/api.github.com/users/{username}/repos 
- POST localhost:8080/api.github.com/users/repos 
- POST localhost:8080/api.github.com/users/{username}/

please for POST calls set the header **Content-Type**: `application/json `

### Data for testing

```sh
$ cd github-api/src/test/resources/
```
- Inside the directory above you will find `github.postman_collection.json` to import in your [POSTMAN](https://www.getpostman.com/) and have all the endpoints, body configured. 


#### Open points and features released in future version

- add Spring security and activate HTTP basic auth reading authorized users from Mongo
- add integration test for the controller using mockMVC and fongo in-memory DB